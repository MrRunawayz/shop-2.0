<?php

/*
    Resulting messages are being generated here

*/
class Result {

    private $success = '';
    private $failure = '';

    public function __construct($success, $error) {
        $this->success = $success;
        $this->failure = "ERROR: Could not able to execute. <br>Reason: " . $error;
    }

    public function showResult(){
        echo "
      <div class='success'>$this->success</div>
      <div class='failure'>$this->failure</div>";
    }

}
?>
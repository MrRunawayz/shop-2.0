<?php
require_once('actions.php');

/*
 * This block validates data from user input. It executes when Save button is clicked
 * If all data is OK then function add from actions class executes
 *  */

$SKUErr = $nameErr = $priceErr = $Err = $attrErr = "";

if((isset($_POST['Save']))) {

    if (empty($_POST["add-SKU"])){
        $SKUErr = "SKU is required";
    }
    elseif (empty($_POST["add-name"]) ){
        $nameErr = "Name is required";
    }
    elseif (empty($_POST["add-price"]) ) {
        $priceErr = "Price is required";
    }
    elseif (empty($_POST["add-size"]) && (empty($_POST["add-dimensions"])) && (empty($_POST["add-weight"]))) {
        $attrErr = "Please make a choice here and fill with data!";
    }
    else {
        $product = new Product();
        $product->setSKU($_POST["add-SKU"]);
        $product->setName($_POST["add-name"]);
        $product->setPrice($_POST["add-price"]);
        $product->setType($_POST['switcher']);

        $product->setSize($_POST["add-size"]);
        $product->setDimensions($_POST["add-dimensions"]);
        $product->setWeight($_POST["add-weight"]);

        $actions = new Actions();
        $actions->add($product);
    }

}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="script.js"></script>
    <link href="style.css" rel="stylesheet" type="text/css" media="screen">
</head>

<body>

<div id="main">
    <div id="header">
        <table id="header-table">
            <tr><td rowspan="2" id="headline-text">Add Product</td>
                <td id="header-right"><button><a href="index.php">Back</a></button></td></tr>
            <tr><td id="header-right"><input type="submit" class="button" form="add-product" name="Save" value="Save" /></td></tr>
        </table>
    </div>

    <hr>

    <div id="content">
        <form action="add-form.php" id="add-product" method="post">
            <table id="add-form">
                <tr><td class="error">* required field</td></tr>
                <tr><td class="message"></td></tr>
                <tr><td>SKU: </td><td><input type="text" name="add-SKU"></td><td class="error">* <?php echo $SKUErr;?></td></tr>
                <tr><td>Name: </td><td><input  type="text" name="add-name"></td><td class="error">* <?php echo $nameErr;?></td></tr>
                <tr><td>Price: </td><td><input type="number" step="0.01" name="add-price"></td><td class="error">* <?php echo $priceErr;?></td></tr>
                <tr><td class="error"></td></tr>
            </table>

            Type switcher:<span class="error">*</span>
            <select id="switcher" class="switcher" name="switcher">
                <option value="dvd-disc">DVD-disc</option>
                <option value="furniture">Furniture</option>
                <option value="book">Book</option>
            </select><br>

            <table id="add-form">
            <tr class="error"><td><?php echo $attrErr;?></td></tr>
            <tr class="for-dvd-disc"><td>Size: </td><td><input id="add-size" type="number" name="add-size"></td></tr>
            <tr class="for-dvd-disc" id="info"><td colspan="2">Please insert numbers only</td></tr>
            <tr class="for-furniture"><td>Dimensions: </td><td><input pattern="[0-9]{1,2}x[0-9]{1,2}x[0-9]{1,2}" step="0.01" type="text" name="add-dimensions"></td></tr>
            <tr class="for-furniture" id="info"><td colspan="2">Please insert numbers in format 00x00x00</td></tr>
            <tr class="for-book"><td>Weight: </td><td><input type="number" step="0.01" name="add-weight"></td></tr>
            <tr class="for-book" id="info"><td colspan="2">Please insert numbers only</td></tr>
            </table>
        </form>

    </div>
</div>

</body>
</html>



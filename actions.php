<?php

require_once 'database.php';
require_once 'result.php';
require_once 'product.php';


class Actions {

    public function __construct(){
    }

    /* Function delete can delete items which user has been clicked */

    public function delete($checkbox){
        $sql = "";
        for($i=0;$i<count($checkbox);$i++){
            $del_id = $checkbox[$i];
            $sql .= "DELETE FROM items WHERE SKU='".$del_id."';";
        }

        $db = new DataBase();
        $conn = $db->connect();

        if ($conn->multi_query($sql) === true) {
            header('Location: ./index.php');
        } else {
            $error = $conn->error;
            $result = new Result(null, $error);
            $result->showResult();
        }
    }

    /* Function add can add new items into database. It is getting values from add-form.php */

    public function add($product){
        $db = new DataBase();
        $conn = $db->connect();

        $SKU = $product->getSKU();
        $name = $product->getName();
        $price = $product->getPrice();
        $size = $product->getSize();
        $weight = $product->getWeight();
        $dimensions = $product->getDimensions();
        $type = $product->getType();

            $sql = "INSERT INTO items (SKU, `name`, price, `type`)
                VALUES ('$SKU', '$name', '$price', '$type');";

            if (!empty($product->getSize())) {
                $sql .= "INSERT INTO attributes (SKU, `key`, `value`)
                        VALUES ('$SKU', 'size', '$size');";
            }
            if (!empty($product->getDimensions())) {
                $sql .= "INSERT INTO attributes (SKU, `key`, `value`)
                        VALUES ('$SKU', 'dimensions', '$dimensions');";
            } else {
                $sql .= "INSERT INTO attributes (SKU, `key`, `value`)
                        VALUES ('$SKU', 'weight', '$weight');";
            }

            /* If there is error, it is being showed to user */

        if ($conn->multi_query($sql) === true) {
            header('Location: ./index.php');
        } else {
            $error = $conn->error;
            $result = new Result(null, $error);
            $result->showResult();
        }
        }


}

?>
<?php

class DataBase {

    private $connection;

    private $server = "localhost";
    private $username = "root";
    private $password = "";
    private $db = "shop";

    public function __construct() {
    }

    public function connect(){
        $this->connection = mysqli_connect($this->server, $this->username, $this->password, $this->db) or die("Couldn't connect");

        return $this->connection;
    }
}
?>
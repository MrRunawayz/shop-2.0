/* Input fields are being showed or hide based on switcher select choice;
 * Switcher is an id for select on add-product.php  *
  * */

$(document).ready(function(){
    $("#switcher").change(function(){
        var type = $('.switcher').val();
        var selected = "." + type;

        if (selected == ".dvd-disc") {
            $(".for-dvd-disc").css("display", "block");
            $(".for-furniture").css("display", "none");
            $(".for-book").css("display", "none");
        }
        else if(selected == ".furniture"){
            $(".for-furniture").css("display", "block");
            $(".for-dvd-disc").css("display", "none");
            $(".for-book").css("display", "none");
        }
        else{
            $(".for-book").css("display", "block");
            $(".for-dvd-disc").css("display", "none");
            $(".for-furniture").css("display", "none");
        }
    });
});

/* Additional function for switcher select menu - it adds blank option as 1st on top  */

$(document).ready(function () {
    $("#switcher").prop("selectedIndex", -1);

});



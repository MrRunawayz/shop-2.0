<?php

class Product {
    private $name;
    private $SKU;
    private $price;
    private $type;
    private $dimensions;
    private $size;
    private $weight;
    private $key;
    private $value;

    public function __construct(){

    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = trim($name);
    }

    public function getSKU(){
        return $this->SKU;
    }

    public function setSKU($SKU){
        $this->SKU = trim($SKU);
    }

    public function getPrice(){
        return $this->price;
    }

    public function setPrice($price){
        $this->price = trim($price);
    }

    public function getType(){
        return $this->type;
    }

    public function setType($type){
        $this->type = trim($type);
    }

    public function getDimensions(){
        return $this->dimensions;
    }

    public function setDimensions($dimensions){
        $this->dimensions = trim($dimensions);
    }

    public function getSize(){
        return $this->size;
    }

    public function setSize($size){
        $this->size = trim($size);
    }

    public function getWeight(){
        return $this->weight;
    }

    public function setWeight($weight){
        $this->weight = trim($weight);
    }

    public function getKey(){
        return $this->key;
    }

    public function getValue(){
        return $this->value;
    }

    public function setKey(){
        return $this->key;
    }

    public function setValue(){
        return $this->value;
    }




}
?>
<?php
include 'product.php';
include 'database.php';

/* This block shows list of items. Each item has it's own div. Divs are being showed using CSS attribute  grid-template-columns  */

$product = new Product();
$list = new ProductsList ();
$list->showProductsList();

    class ProductsList{

        public function __construct(){

        }

        public function showProductsList(){

            /* Create new connection */

            $database = new DataBase();
            $db = $database->connect();

            /* Get existing values from database */

            $sql = "SELECT
                    attributes.SKU,
                    items.name,
                    items.price,
                    items.type,
                    attributes.key,
                    attributes.value
                    from Items LEFT JOIN Attributes on (attributes.SKU = items.SKU)";


            $result = $db->query($sql);
            $data = array();

            /* Retrieve values as results of get methods */

            while ($dbProduct = mysqli_fetch_object($result, 'Product')) {

                $data[$dbProduct->getSKU()]["SKU"] = $dbProduct->getSKU();
                $data[$dbProduct->getSKU()]["Name"] = $dbProduct->getName();
                $data[$dbProduct->getSKU()]["Price"] = $dbProduct->getPrice();
                $data[$dbProduct->getSKU()]["Type"] = $dbProduct->getType();
                $data[$dbProduct->getSKU()]["attributes"][$dbProduct->getKey()] = [$dbProduct->getValue()];
            }

            /* Echo results in divs */

            echo "<form action='index.php' class='check-form' id='delete-form' method='post'>";
            foreach ($data as $value) {
                $val = $value["SKU"];
                echo "<div id='item'><br><input class='checkbox' type='checkbox' name='checkbox[]' value='$val'>";
                echo "<p>SKU: " . $value["SKU"] . "</p>";
                echo "<p>Name: " . $value["Name"] . "</p>";
                echo "<p>Price: " . $value["Price"] . " $</p>";
                if (strval($value["Type"]) == "furniture") {
                    echo "<p>Dimensions: " . implode($value["attributes"]["dimensions"]) . "</p>";
                } elseif (strval($value["Type"]) == "dvd-disc") {
                    echo "<p>Size: " . implode($value["attributes"]["size"]) . " MB</p>";
                } else {
                    echo "<p>Weight: " . implode($value["attributes"]["weight"]) . " KG</p>";
                }
                echo "<br></div>";

            }


            echo "</form>";
        }
    }
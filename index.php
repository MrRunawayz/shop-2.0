<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="script.js"></script>
    <link href="style.css" rel="stylesheet" type="text/css" media="screen">
</head>

<body>

<div id="main">
    <div id="header">
        <table id="header-table">
            <tr><td rowspan="2" id="headline-text">Product List</td><td id="header-right"><button><a href="add-form.php">Add product</a></button></td></tr>
            <tr><td id="header-right">
                <input class="button" id="delete" type="submit" name="delete" value="Delete" form="delete-form"/>
            </td></tr>
        </table>
    </div>

    <hr>

    <div id="content">
            <?php include ('products-list.php'); ?>
    </div>
</div>


</body>
</html>

<?php
require_once('actions.php');

/* This block starts delete function if delete button was clicked */

if((isset($_POST['delete'])) && isset($_POST['checkbox'])) {
    $actions = new Actions();
    $actions->delete($_POST['checkbox']);
}
?>